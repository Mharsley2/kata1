// Return the numbers from 1 to 20. (1, 2, 3,…, 19, 20)

for (var i = 1; i <= 20; i++) {
    console.log(i);
}
// Return the even numbers from 1 to 20. (2, 4, 6,…18,20)

function printEven() {
    for (let i = 1; i < 20; i++) {
        if (i % 2 === 0) {
            console.log(i);
        }
    }
}

printEven();

// Return the odd numbers from 1 to 20. (1, 3, 5,…,17,19)

function printOdd() {
    for (let i = 1; i < 20; i++) {
        if (i % 2 !== 0) {
            console.log(i);
        }
    }
}

printOdd();
// Return the multiples of 5 up to 100. (5, 10, 15, …, 95, 100)

for (var i = 5; i <= 100; i++) {
    if (i % 5 == 0) console.log(i);
}

// Return the square numbers up to 100. (1, 4, 9, …, 81, 100)

function squares() {
    for (let i = 1; i <= 10; i++) {
        console.log(i * i)
    }



}
squares();

// Return the numbers counting backwards from 20 to 1. (20, 19, 18, …, 2, 1)
function down() {
    for (let i = 20; i >= 1; i--) {
        console.log(i)
    }
}
down();
// Return the even numbers counting backwards from 20. (20, 18, 16, …, 4, 2)
function evenDown() {
    for (let i = 20; i >= 1; i -= 2) {
        console.log(i)
    }
}
evenDown();
// Return the odd numbers from 20 to 1, counting backwards. (19, 17, 15, …, 3, 1)
function oddDown() {
    for (let i = 19; i >= 1; i -= 2) {
        console.log(i)
    }
}

oddDown();

// Return the multiples of 5, counting down from 100. (100, 95, 90, …, 10, 5)
function five() {
    for (let i = 100; i >= 1; i -= 5) {
        console.log(i)
    }
}
five();
// Return the square numbers, counting down from 100. (100, 81, 64, …, 4, 1)
function root() {
    for (let i = 10; i >= 1; i--) {
        console.log(i * i)
    }
}
root();

